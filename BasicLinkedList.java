package listClasses;


import java.util.Iterator;
/**
 * BasicLinkedList creates a singly linked list. This class 
 * Enables the users to add elements to the front and the end 
 * of the list. Additionally it only lets access to those in 
 * the front and the end. 
 * 
 * 
 * @author Shubham Patel
 * 
 */
public class BasicLinkedList<T> implements Iterable<T> {

	
	protected Node head;  // Front of the list
	protected Node tail; // End of the list
	protected int size; // Size of the list

	
	/**
	 * Class constructor, initializes the class variables
	 * head and tail at null.
	 */
	public BasicLinkedList() {
		head = null;
		tail = null;
		size = 0;
	}
	
	/**
	 * Basic getter for the size of the list
 	 * @return returns a size int of the list. 
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Adds the data/object to the end of the list. 
	 * @param data Type T data/object
	 * @return The BasicLinkedList current object
	 */
	
	public BasicLinkedList<T> addToEnd(T data) {

		Node added = new Node(data, null);
		// if this is the first object of the
		// list, it becomes the head;
		if (size == 0) {
			head = added;
			size++;
			return this;
		}

		// otherwise iterate through the list
		// and attach the reference of the node with
		// data passed to the last node
		else {
			Node current = head;
			while (current.next != null) {
				current = current.next;
			}

			current.next = added;

			tail = added;
			size++;
			return this;
		}

	}
	
	/**
	 * Adds data/object to the front of the list
	 * 
	 * @param data
	 * @return
	 */

	public BasicLinkedList<T> addToFront(T data) {
		Node added = new Node(data, head);
		// if this is the first data added to the list
		// the added node is both head and tail
		if (size == 0) {
			head = added;
			tail = added;
		}

		// otherwise the added node will reference
		// current head, and then be set as the head
		else {
			added.setNode(head);
			head = added;
		}
		size++;

		return this;
	}
	
	/**
	 * getter for the first element of the list.
	 * 
	 * @return The first element of the list.
	 */

	public T getFirst() {
		return head.getObject();
	}
	
	/**
	 * Getter for the last element of the list
	 * 
	 * @return The last element of the list
	 */
	public T getLast() {
		Node cur = head;
		// iterates through the whole list
		// return the last node's object
		while (cur.getNext() != null) {
			cur = cur.getNext();
		}

		return cur.getObject();
	}

	/**
	 * Retrieves the first element of the list 
	 * as well as removing it from the list
	 * @return The first element
	 */
	public T retrieveFirstElement() {
		Node temp;
		Node secondNode = head.getNext();
		temp = head;

		// sets the reference of the first element
		// as null. Then makes the secondNode as the
		// head of the list.
		head.setNode(null);
		head = secondNode;
		size--;
		return temp.getObject();

	}
	
	/**
	 * Retrieves the last element of the list
	 * as well as removing it from the list.
	 * @return The last element 
	 */

	public T retrieveLastElement() {

		Node cur = head;
		Node prv = null;

		// iterates through the entire list.
		while (cur.getNext() != null) {
			prv = cur;
			cur = cur.next;
		}

		// sets the second-to-last node's reference
		// as null, and returns the last node's object
		Node last = cur;
		Node secondLast = prv;
		secondLast.next = null;

		size--;
		return last.getObject();
	}
	
	/**
	 * Removes every single instance of the data passed in,
	 * Additionally a comparator for the object type will be 
	 * needed.
	 * @param targetData Data that needs to be removed
	 * @param comparator comparator for the data's object type
	 * @return current BasicLinkedList object
	 */

	public BasicLinkedList<T> remove(T targetData,
			java.util.Comparator<T> comparator) {

		// if there is nothing in the list
		// Doesn't do anything;
		if (size == 0) {
			return this;
		}

		// if head the same as targetData then the nest node becoms head
		else if (comparator.compare(targetData, head.getObject()) == 0) {
			head = head.next;
		}

		Node cur = head;
		Node prv = head;

		// Traverse the list
		while (cur != null) {
			// if the head is the same as the targetData then
			// the next node becomes the head
			if (cur == head
					&& comparator.compare(targetData, head.getObject()) == 0) {
				head = head.next;
			}

			// checks if any after head is the same as targetData
			// if they are, then the previous node's reference
			// will be set as the next node.
			else if (comparator.compare(targetData, cur.getObject()) == 0
					&& prv.next != null) {
				
				if (cur.next == null) {
					
					prv.next = null;
					tail = prv;
					size--;
					return this;
				}
				
				prv.next = cur.next;
				cur = cur.next;
			} else {
				prv = cur;
				cur = cur.next;
			}
		}
		return this;
	}
	
	/**
	 * Iterator for the BasicLinkedList class
	 */
	public java.util.Iterator<T> iterator() {

		// an anonymous class with iterator methods
		return new Iterator<T>() {

			Node nxNode = head;

			@Override
			public boolean hasNext() {
				// if the list is empty
				// return false.
				if (head == null) {
					return false;
				}
				// if the current node is null
				// return false
				else if (nxNode == null) {
					return false;
				}

				return true;
			}

			@Override
			public T next() {
				// sets the current node as
				// the next node
				// returns the current node's
				// objects
				if (nxNode != null) {
					T data = nxNode.getObject();
					nxNode = nxNode.next;

					return data;
				}
				return null;
			}
		};
	}

	// Method made, solely to test weather the list contains a
	// Particular element. This method is used for JUnit tests only.
	public boolean contains(T ele) {
		Node cur = head;

		while (cur != null) {

			if (cur.getObject().equals(ele)) {

				return true;
			}

			cur = cur.next;
		}

		return false;
	}
	

	
	///////////////////////////////////////////////////////////////////////

	                           /*INNER CLASSES*/

	///////////////////////////////////////////////////////////////////////

	// node class
	public class Node {

		T obj;
		Node next;

		// The node takes in any type of data,
		// as well as the reference to the next element
		public Node(T object, Node node) {
			this.obj = object;
			this.next = node;

		}

		// getter for the object in node
		public T getObject() {
			return obj;
		}

		// getter for the next node
		public Node getNext() {
			return next;
		}

		// setter for the next node
		public void setNode(Node node) {
			this.next = node;
		}

	}
}
