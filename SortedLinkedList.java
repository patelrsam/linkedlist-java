package listClasses;

import java.util.Comparator;

public class SortedLinkedList<T> extends BasicLinkedList<T> {

	Comparator<T> compare;

	/**
	 * Class constructor, sets super class's variables to null
	 * and sets the comparator to the one passed in.
	 * 
	 * @param comprator comparator for the list's type
	 */
	public SortedLinkedList(java.util.Comparator<T> comprator) {
		super();
		head = null;
		tail = null;
		size = 0;
		compare = comprator;
	}

	/**
	 * Add the element in the correct position in the list
	 * @param element data that is to be added
	 * @return current SortedLinkedList object
	 */
	public SortedLinkedList<T> add(T element) {
		Node added = new Node(element, null);
		// if the list is empty, theadded node is the head
		if (size == 0) {
			head = added;
		}

		else if (this.getSize() == 1) {
			// if there is only one element in the list
			// adds the node in from or behind the element.
			if (compare.compare(element, head.getObject()) > 0) {
				head.next = added;
			} else if (compare.compare(element, head.getObject()) <= 0) {
				added.next = head;
				head = added;
			}
		} else {
			Node cur = head;
			Node prv = null;
			// traverse the list
			while (cur.getNext() != null) {
				// if the passed in object is less then the current,
				// added the passed in node behind the current
				if (compare.compare(element, cur.getObject()) < 0) {
					if (cur == head) {
						added.next = head;
						head = added;
						prv = cur;
						cur = cur.getNext();
						break;
					} else {
						added.next = cur;
						prv.next = added;

						prv = cur;
						cur = cur.getNext();
						break;
					}
				}
				prv = cur;
				cur = cur.getNext();
			}
			// checks if the current node is less or equal to current node
			if (compare.compare(element, cur.getObject()) >= 0) {
				cur.next = added;
				added.next = null;
				tail = added;
			}
			// checks if the passed node is less then the current
			// but greater then the previous node
			else if (compare.compare(element, prv.getObject()) >= 0
					&& compare.compare(element, cur.getObject()) <= 0) {
				added.next = cur;
				prv.next = added;
			}
		}
		size++;
		return this;
	}

	/**
	 * If addToEnd method is used, UnsupportedOperationException is thrown
	 * 
	 * @exception UnsupportedOperationException 
	 */
	public BasicLinkedList<T> addToEnd(T data) {
		throw new UnsupportedOperationException(
				"Invalid operation for sorted list.");
	}

	/**
	 * If addToFront method is used, UnsupportedOperationException is thrown
	 * 
	 * @exception UnsupportedOperationException 
	 */
	public BasicLinkedList<T> addToFront(T data) {
		throw new UnsupportedOperationException(
				"Invalid operation for sorted list.");
	}
	/**
	 * Removes all instances of the data passed in for the list
	 * @param targetData Data to be removed
	 * @return current SortedLinkedList object
	 */
	public BasicLinkedList<T> remove(T targetData) {
		//uses the parent class's remove method 
		return super.remove(targetData, compare);

	}

}
